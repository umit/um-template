var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minifycss = require('gulp-minify-css');
var less = require('gulp-less');
var path = require('path');

gulp.task('less', function () {
    return gulp.src('less/site-templates/um-template.less')
        .pipe(less({
            paths: [path.join(__dirname, 'less', 'includes')]
        }))
        .pipe(concat('um-template.min.css'))
        .pipe(minifycss())
        .pipe(gulp.dest('../_plugins/css'));
});

gulp.task('bucket', function () {
    return gulp.src('less/buckets/theme-page.less')
        .pipe(less({
            paths: [path.join(__dirname, 'less', 'includes')]
        }))
        .pipe(concat('theme-page.min.css'))
        .pipe(minifycss())
        .pipe(gulp.dest('../_plugins/css'));
});

gulp.task('watch', function() {
    gulp.watch('less//**/*.less', ['less']);
});

gulp.task('default', ['less','bucket']);